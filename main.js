let input1DOM = document.getElementById("input1")
let input2DOM = document.getElementById("input2")

function tambah(){
    let hasilluas = parseInt(input1DOM.value) + parseInt(input2DOM.value)
    document.getElementById('hasil').value = hasilluas
}

function kurang(){
    let hasiluas = parseInt(input1DOM.value) - parseInt(input2DOM.value)
    document.getElementById('hasil').value = hasiluas
}

function bagi(){
    let hasiluas = parseInt(input1DOM.value) / parseInt(input2DOM.value)
    document.getElementById('hasil').value = hasiluas
}

function modulus(){
    let hasiluas = parseInt(input1DOM.value) % parseInt(input2DOM.value)
    document.getElementById('hasil').value = hasiluas
}

function kali(){
    let hasiluas = parseInt(input1DOM.value) * parseInt(input2DOM.value)
    document.getElementById('hasil').value = hasiluas
}

function pangkat2(){
    let hasiluas = parseInt(input1DOM.value) ** parseInt(input2DOM.value)
    document.getElementById('hasil').value = hasiluas
}

function akar2(){
    let hasiluas = Math.sqrt(input1DOM.value)
    document.getElementById('hasil').value = hasiluas
}

function akar3(){
    let hasiluas = Math.cbrt(input1DOM.value)
    document.getElementById('hasil').value = hasiluas
}

function reset(){
    input1DOM.value = "";
    input2DOM.value = "";

    location.reload();
}
